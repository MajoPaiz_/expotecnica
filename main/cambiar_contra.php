<?php
require("../lib/page.php");
Page::header("Cambiar contraseña");
//selecciona el cliente
$sql = "SELECT * FROM clientes";
$data = Database::getRows($sql, null);
if($data == null)
{
    header("location: ../main/register.php");
}

if (isset($_SESSION['codigo_cliente']) && isset($_SESSION['verifiacion_usuario']))
{
    if ($_SESSION['verifiacion_usuario'] == 1)
    {
        //valida si el post esta vacio y enlaza las variables con el campo
        if(!empty($_POST))
        {
            $_POST = validator::validateForm($_POST);
            $clave1 = $_POST['clave1'];
            $clave2 = $_POST['clave2'];
            try 
            {
                //validacion de clave
                if($clave1 != "" && $clave2 != "")
                {
                    if($clave1 == $clave2)
                    {
                 /*aqui validar*/       if (preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $clave1))
                        {
                            
                            //ingreso de datos de usuario
                            $clave = password_hash($clave1, PASSWORD_DEFAULT);
                            //actializa un registro existente
                            $sql = "UPDATE clientes SET clave = ? WHERE codigo_cliente = ?";
                            $params = array($clave, $_SESSION['codigo_cliente']);
                            if(Database::executeRow($sql, $params))
                            {
                                $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
                                $params = array($_SESSION['codigo_cliente']);
                                $data = Database::getRow($sql, $params);
                                if ($data != null){
                                    $_SESSION['codigo_cliente'] = $data['codigo_cliente'];
			      					$_SESSION['nombres_cliente'] = $data['alias'];
                                    Page::showMessage(1, "Contraseña actualizada correctamente", "index.php");
                                }
                                else 
                                {
                                    throw new Exception("Ocurrio un error de seguridad.");
                                }
                            }
                                
                        }
                        else 
                        {
                            throw new Exception("El formato de contraseña incorrecto. La contraseña debe contener por lo menos un número y un caracter especial (Ejemplo: Abcdef1#)");
                        }/*aqui termina*/
                    }
                    else
                    {
                        throw new Exception("Las contraseñas no coinciden");
                    }
                }
                else {
                    throw new Exception("Debe ingresar ambas contraseñas");
                }
                                        
            }
            catch (Exception $error)
            {
                Page::showMessage(2, $error->getMessage(), null);
            }
        }
    } else {
        Page::showMessage(2, "No se ha verificado la cuenta", "verificar.php");
        header("location: verificar.php");
    }
}
?>

<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='clave1' type='password' name='clave1'pattern="[A-Z,a-z,0-9,_-,{1,15}]" require  class="validate" required/>
			<label for='clave1'>Contraseña</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='clave2' type='password' name='clave2'pattern="[A-Z,a-z,0-9,_-,{1,15}]" require  class="validate" required/>
			<label for='clave2'>Confirmar contraseña</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>

<?php
Page::footer();
?>