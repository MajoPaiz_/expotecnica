<?php
require("../lib/page.php");
include "../lib/phpmailer/class.phpmailer.php";
include "../lib/phpmailer/class.smtp.php";

Page::header("Recuperar contraseña");
// selecciona el cliente para recuperar contraseña
$sql = "SELECT * FROM clientes";
$data = Database::getRows($sql, null);
if($data == null)
{
    header("location: ../main/register.php");
}
//valida la informacion seleccionada 
if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$correo = $_POST['correo'];
  	try
    {
      	if($correo != "")
  		{
  			$sql = "SELECT * FROM clientes WHERE email_cliente = ?";
		    $params = array($correo);
		    $data = Database::getRow($sql, $params);
		    if($data != null)
		    {
				$codigo = $data['codigo_cliente'];
				$clave_provicional = Validator::generarCodigo(8);
				$clave = password_hash($clave_provicional, PASSWORD_DEFAULT);
				$sql = "UPDATE clientes set clave = ? WHERE email_cliente = ? AND codigo_cliente = ?";
                $params = array($clave, $correo, $codigo);
				Database::executeRow($sql, $params);
				$email_user = "";
				$email_password = "";
				$the_subject = "Recuperar cuenta";
				$address_to = $correo;
				$from_name = "Rockoshop";
				$phpmailer = new PHPMailer();
				// ---------- datos de la cuenta de Gmail -------------------------------
				$phpmailer->Username = $email_user;
				$phpmailer->Password = $email_password; 
				//-----------------------------------------------------------------------
				// $phpmailer->SMTPDebug = 1;
				$phpmailer->SMTPSecure = 'ssl';
				$phpmailer->Host = "smtp.gmail.com"; // GMail
				$phpmailer->Port = 465;
				$phpmailer->IsSMTP(); // use SMTP
				$phpmailer->SMTPAuth = true;
				$phpmailer->setFrom($phpmailer->Username,$from_name);
				$phpmailer->AddAddress($address_to); // recipients email
				$phpmailer->Subject = $the_subject;	
				$phpmailer->Body .="<h1 style='color:#3498db;'>Hola </h1>";
				$phpmailer->Body .= "<p>Tu codigo es: ".$clave_provicional."</p>";
				$phpmailer->Body .= "<p>Fecha y Hora: ".date("d-m-Y h:i:s")."</p>";
				$phpmailer->IsHTML(true);
				
				try {
					$phpmailer->Send();
					$_SESSION['codigo_cliente'] = $data['codigo_cliente'];
					$_SESSION['verifiacion_usuario'] = 0;
					header('location: verificar.php');                             
				} catch (Exception $exec){
					Page::showMessage(2, $error->getMessage(), null);
				}
			}
		    else
		    {
		    	throw new Exception("El usuario ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un alias y una clave");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- se crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input type="email" id="correo" name="correo" class="validate"autocomplete="off" required/>
            <label for="correo">Correo eléctronico:</label>
		</div>
		
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>

<?php
Page::footer();
?>