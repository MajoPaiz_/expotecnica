<?php
require("../lib/page.php");
Page::header("Editar perfil");
/*formulario de editar perfil */
if(!empty($_POST))
{/* se declaran las variables que se quiere ingresar*/
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_cliente'];
  	$apellidos = $_POST['apellidos_cliente'];
    $correo = $_POST['email_cliente'];
    $alias = $_POST['alias'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];

    try 
    {
      	if($nombres != "" && $apellidos != "")
        {
            if(Validator::validateEmail($correo))
            {
                if($alias != "")
                {
                    if($clave1 != "" || $clave2 != "")
                    {
                        if($clave1 == $clave2)
                        {
                            if (preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $clave1))
                            {
                                /*la consulta de update del perfil*/
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "UPDATE clientes SET nombres_cliente = ?, apellidos_cliente = ?, email_cliente = ?, alias = ?, clave = ? WHERE codigo_cliente = ?";
                                $params = array($nombres, $apellidos, $correo, $alias, $clave, $_SESSION['codigo_cliente']);
                            }
                            else
                            {
                                throw new Exception("El formato de contraseña incorrecto. La contraseña debe contener por lo menos un número y un caracter especial (Ejemplo: Abcdef1#)");
                            }
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE clientes SET nombres_cliente = ?, apellidos_cliente = ?, email_cliente = ?, alias = ? WHERE codigo_cliente= ?";
                        $params = array($nombres, $apellidos, $correo, $alias, $_SESSION['codigo_cliente']);
                    }
                    if(Database::executeRow($sql, $params))
                    {
                        Page::showMessage(1, "Operación satisfactoria", "index.php");
                    }
                    else
                    {
                        throw new Exception("Operación fallida");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar un alias");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
    $params = array($_SESSION['codigo_cliente']);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_cliente'];
    $apellidos = $data['apellidos_cliente'];
    $correo = $data['email_cliente'];
    $alias = $data['alias'];
}
?>
  <div class="container">
<!-- se crea el formulario -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_cliente' type='text' name='nombres_cliente' class='validate' autocomplete="off"value='<?php print($nombres); ?>' required/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_cliente' type='text' name='apellidos_cliente' class='validate'autocomplete="off"autocomplete="off" value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_cliente'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_cliente' type='email' name='email_cliente' class='validate' autocomplete="off" value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' autocomplete="off" value='<?php print($alias); ?>' required/>
            <label for='alias'>Alias</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR CLAVE</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' autocomplete="off" class='validate'/>
            <label for='clave1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate'/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='../main/index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
    </div>

</form>

<?php
Page::footer();
?>