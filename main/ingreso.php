<?php
require("../lib/page.php");
Page::header("Tus ingresos");
$sql2 = "SELECT count(*) AS cantidad  FROM ingreso, clientes WHERE ingreso.codigo_cliente = clientes.codigo_cliente  ORDER BY fecha_ingreso";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=10;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
}
	/*inicio de formulario evento*/
 
// if de tiempo 
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
	//es para compartir los seleccionado 

	$codigito=$_SESSION['codigo_cliente'];
	$sql = "SELECT * FROM ingreso, clientes WHERE ingreso.codigo_cliente = clientes.codigo_cliente AND clientes.codigo_cliente=? ORDER BY fecha_ingreso limit $inicio,$properpag" ;
	$params = array($codigito);
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- para poner toda el data y con eso se muestra la tabla -->
<div class="container">
<form method='post'>
	
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>Fecha ingreso</th>
			<th>Hora ingreso</th>
			<th>Sistema</th>
            <th>Navegador</th>
						
			<th>ingresos</th>
			<th>baneos</th>

		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['fecha_ingreso']."</td>
				<td>".$row['hora_ingreso']."</td>
                
                <td>".$row['sistema']."</td>
				<td>".$row['navegador']."</td>
				<td><a href='../public/reportes/reporte_ingreso.php?id=".$row['codigo_cliente']."' class='btn waves-effect yellow'><i class='material-icons'>thumb_up</i></a></td>
				<td><a href='../public/reportes/reporte_baneado.php?id=".$row['codigo_cliente']."'class='btn waves-effect red'><i class='material-icons'>report</i></a></td>
			
				<td>
		");
		}
	}
	print("
		</tbody>
	</table>
	</div>
	"); //Fin de if que comprueba la existencia de registros.
?>


<div class="row center aling">
	<ul class="pagination">
	<?php
	if($norpag>1)
	{
			echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
	}
	for($i=1;$i<=$canpag;$i++)
	{
		if($i==$norpag)
		{ 

			   echo "  <li class='active green'><a >$i</a></li>";
		}
		else
		{
			   echo "<li class='waves-effect'><a  href='ingreso.php?num=$i'>$i</a></li>";   
		}
	}
	if($norpag<$canpag-1)
	{
			echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
	}
	?>
	 </ul>
	 
	</div>

<?php
Page::footer();
?>