<?php
require("../lib/page.php");
if(empty($_GET['id'])) 
/*formulario de registor en el sitio publico */

{
    Page::header("Agregar cliente");
    $id = null;
    $nombres = null;
    $apellidos = null;
    $alias = null;
    $imagen = null;
    $correo= null;
    $telefono = null;

}
else
{
    Page::header("Modificar cliente");
    $id = $_GET['id'];
    $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_cliente'];
    $apellidos = $data['apellidos_cliente'];
    $alias = $data['alias'];
    $imagen = $data['imagen_perfil'];
    $correo = $data['email_cliente'];
    $telefono = $data['telefono_cliente'];
    }

if(!empty($_POST))
{/*declaracion de valiables a ocupar en el formulario para registrar  un cliente en el sitio publico*/
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_cliente'];
    $apellidos = $_POST['apellidos_cliente'];
    $archivo = $_FILES['imagen'];
  	$correo = $_POST['email_cliente'];
    $telefono = $_POST['telefono_cliente'];
    $copia=$_POST['copia'];
    $captcha=$_POST['captcha'];
    try 
    {
    if($copia == $captcha)
    {

        if($archivo['name'] != null)
                            {
                                $imagen = Validator::validateImage($archivo, 1200, 1200);
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
         	if($nombres != "" && $apellidos != "")
        {
            if(Validator::validateEmail($correo))
            {
                if($id == null)
                {
                    $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                /*consulta del insert en la tabla de clientes*/
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO clientes (nombres_cliente, apellidos_cliente, alias, imagen_perfil, clave, email_cliente, telefono_cliente) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $alias,$imagen, $clave, $correo,$telefono);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un alias");
                    }
                }
                else
                { 
                
                $sql = "UPDATE clientes SET nombres_cliente= ?, apellidos_cliente= ?, alias= ?, imagen_perfil= ?,email_cliente= ?,telefono_cliente= ? WHERE codigo_cliente = ?";
                    $params = array($nombres, $apellidos, $alias,$imagen, $correo,$telefono, $id);
                }
                if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "../index.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
        else
    {
        throw new Exception("Codigo no conincide");
    }    
}     


    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

  <!--parallax-->
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <h1 class="header center"><img src="../img/logo.png" width="300" height="250" ></h1>
        <div class="row center">
          <h3 class="header col s12  verdeeee">Registrate</h3>
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/sliderg4.jpg" alt="Unsplashed background img 1"></div>
  </div>
                          
<script type="application/x-javascript">
function validar(formulario){
      if (formulario.dato.value.length != 8)
           { alert("Debe introducir una contraseña de 10 dígitos")}
}
</script> 




</script>
  <?php

		function codigo_captcha(){

				$k="";
				$paramentros="1234567890abcdefghijkmnopqstuvwxyzABCDEFGHIJKMNOPQSTUVWXYZ";
				$maximo=strlen($paramentros)-1;

				for($i=0; $i<7; $i++){

					$k.=$paramentros{mt_rand(0,$maximo)};

				}

				return $k;

		}

?>  

<form method='post' enctype='multipart/form-data'>
  <div class="container">
<div class='row'>
        
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input autocomplete="off" id='nombres_cliente' type='text' name='nombres_cliente' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input autocomplete="off" id='apellidos_cliente' type='text' name='apellidos_cliente' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_cliente'>Apellidos</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input autocomplete="off" id='email_cliente' type='email' name='email_cliente' class='validate' value='<?php print($correo); ?>' required/>
            <label for='email_cliente'>Correo</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input autocomplete="off" id='alias' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>
        
        <div class='input-field col s12 m6' method=post onSubmit="return validar(this)">
          	<i class='material-icons prefix'>call</i>
          	<input autocomplete="off" id='telefono_cliente' type='text'   maxlength="8" size="8"   name='telefono_cliente' class='validate'  value='<?php print($telefono); ?>' required/>
          	<label for='telefono_cliente'>Telefono</label>
        </div>

      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input  type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
        
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>

    </div>
    <div class='row center'>
    
 <div class='input-field col s12 m4'>

	<tr>
		<td>
			<input type="text" name="copia"   class='validate' required/>
            <label for='copia'>codigo</label></td>
        </div>
         <div class='input-field col s12 m6'>

		<td>
			<input type="text" name="captcha"  value=<?php echo codigo_captcha(); ?> class="captcha" size="4" readonly>
		</td>
	</tr>
    </div>	
	<tr>
		<td>&nbsp;</td>
	</tr>
    </div>

    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1'autocomplete="off" type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' autocomplete="off" class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
        </div>
</form>

<?php
Page::footer();
?>