<?php
require("../lib/page.php");
Page::header("Reservaciones");

$sql2 = "SELECT count(*) AS cantidad  FROM reservaciones, clientes, sucursales WHERE reservaciones.codigo_cliente = clientes.codigo_cliente AND reservaciones.codigo_sucursal = sucursales.codigo_sucursal  ORDER BY alias";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=5;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
}
// se realiza la busqueda del reservacion
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM reservaciones, clientes, sucursales WHERE reservaciones.codigo_cliente = clientes.codigo_cliente AND codigo_cliente=? AND reservaciones.codigo_sucursal = sucursales.codigo_sucursal AND alias LIKE ? ORDER BY alias";
	$params = array($_SESSION['codigo_cliente'],"%$search%");
}
else
{
	$codigito=$_SESSION['codigo_cliente'];
	$sql = "SELECT * FROM reservaciones, clientes, sucursales WHERE reservaciones.codigo_cliente = clientes.codigo_cliente AND clientes.codigo_cliente=? AND reservaciones.codigo_sucursal = sucursales.codigo_sucursal  ORDER BY alias limit $inicio,$properpag";
	$params = array($codigito);
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!--formulario para mostrar las reservaciones -->
<div class='container'>
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='../public/Reservaciones.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form><table class='striped'>
	
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>FECHA</th>
			<th>HORA</th>
			<th>SUCURSAL</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['fecha_reservacion']."</td>
				<td>".$row['hora_reservacion']."</td>
				<td>".$row['nombre_sucursal']."</td>
				<td>
		");
		if($row['estado_reservacion'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='../public/reservaciones.php?id=".$row['codigo_reservacion']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='deletereserva.php?id=".$row['codigo_reservacion']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	</div>
	");

} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "../public/Reservaciones.php");
}
?>
<div class="row center aling">
<ul class="pagination">
<?php
if($norpag>1)
{
		echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
}
else
{
	  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
}
for($i=1;$i<=$canpag;$i++)
{
	if($i==$norpag)
	{ 

		   echo "  <li class='active green'><a >$i</a></li>";
	}
	else
	{
		   echo "<li class='waves-effect'><a  href='ingreso.php?num=$i'>$i</a></li>";   
	}
}
if($norpag<$canpag-1)
{
		echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
}
else
{
	  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
}
?>
 </ul>
 
</div>

<?php
Page::footer();
?>