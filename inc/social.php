<!-- hace que se muestra las barras de las redes sociales -->
<div class="fixed-action-btn vertical click-to-toggle ">
    <a class="btn-floating btn-large  green lighten-1">
      <i class="material-icons">gamepad</i>
      
    </a>
    <ul>
      <li><a class="btn-floating light-blue darken-4 center pulse " href="https://www.facebook.com/losparados/?fref=ts" target="_blank"><img src="../img/png/002-facebook.png" width="40" height="40"></a></li>
      <li><a class="btn-floating red pulse" href="https://www.instagram.com/losparadossv/"  target="_blank"><img src="../img/png/001-instagram-1.png" width="40" height="40"></a></li>
      
    </ul>
  </div>
        