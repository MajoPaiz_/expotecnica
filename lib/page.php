<?php
require("../lib/database.php");
require("../lib/validator.php");
require("../lib/validarban.php");
include("../inc/social.php");
class Page
{
	// funcion para poner las referencias CSS
	public static function header($title)
	{
		session_start();
		print("
			<!DOCTYPE html>
			<html lang='es'>
			<head>
			<meta charset='utf-8'>
				<title>Los parados - $title</title>
				<link type='text/css' rel='stylesheet' href='../css/materialize.css'/>
				<link type='text/css' rel='stylesheet' href='../css/style.css'/>
				<link type='text/css' rel='stylesheet' href='../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../css/icons.css'/>
				
				<link href='../css/introjs.css' rel='stylesheet'>
				<script type='text/javascript' src='../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");
		if(isset($_SESSION['nombres_cliente']))
		{
			print("
				<header class='navbar-fixed'>
					<nav class='green'>
						<div class='nav-wrapper'>

							<a href='#' data-activates='mobile' class='button-collapse'><i class='material-icons'>menu</i></a>
							<ul class='right hide-on-med-and-down  blue-text' >
						
							<li ><a href='../public/index.php'><i class='material-icons left'>home</i>Home</a></li>
							<li><a href='../public/Menu.php'><i class='material-icons left'>receipt</i>Menu</a></li>
							<li><a href='../public/Sucursales.php'><i class='material-icons left'>business</i>Sucursales</a></li>
							<li><a href='../public/Nosotros.php'><i class='material-icons left'>group</i>Nosotros</a></li>
							<li><a href='../public/Contactanos.php'><i class='material-icons left'>call</i>Contactanos</a></li>
							<li><a href='../public/Eventos.php'><i class='material-icons left'>explicit</i>Eventos</a></li>
							<li><a href='../public/Reservaciones.php'><i class='material-icons left'>explicit</i>Reservaciones</a></li>
							
							<li><a class='dropdown-button' href='#' data-activates='dropdown'><i class='material-icons left'>verified_user</i>".$_SESSION['nombres_cliente']."</a></li>
				                
							</ul>
							<ul id='dropdown' class='dropdown-content'>
								<li><a href='../main/profile.php'>Editar perfil</a></li>
								<li><a href='../main/reservaciones.php'>Reservacion</a></li>
								<li><a href='../main/ingreso.php'>Ingreso</a></li>
								<li><a href='../main/Evento.php'>Eventos</a></li>
								<li><a href='../main/logout.php'><i class='material-icons left'>clear</i>Salir</a></li>
							</ul>
						</div>
					</nav>
				</header>
				<ul class='side-nav' id='mobile'>
	               				<li><a href='../public/index.php'><i class='material-icons left'>home</i>Home</a></li>
								<li><a href='../public/Menu.php'><i class='material-icons left'>receipt</i>Menu</a></li>
							    <li><a href='../public/Sucursales.php'><i class='material-icons left'>business</i>Sucursales</a></li>
								<li><a href='../public/Nosotros.php'><i class='material-icons left'>group</i>Nosotros</a></li>
								<li><a href='../public/Contactanos.php'><i class='material-icons left'>call</i>Contactanos</a></li>
								<li><a href='../public/Eventos.php'><i class='material-icons left'>explicit</i>Eventos</a></li>
								<li><a class='dropdown-button' href='#' data-activates='dropdown-mobile'><i class='material-icons'>verified_user</i>".$_SESSION['nombres_cliente']."</a></li>
				</ul>
				<ul id='dropdown-mobile' class='dropdown-content'>

				
					<li><a href='../main/profile.php'>Editar perfil</a></li>
					<li><a href='../main/reservaciones.php'>reservaciones</a></li>
					<li><a href='../main/logout.php'>Salir</a></li>
				</ul>
				<main>
					<h3 class='center-align'>".$title."</h3>
			");
		}
	
		else
		{
			print("
				<header class='navbar-fixed'>
					<nav class='green'>
						<div class='nav-wrapper'>
								<a href='#' data-activates='mobile' class='button-collapse'><i class='material-icons'>menu</i></a>
							<ul class='right hide-on-med-and-down'>
						     	<li><a href='../public/indexpublic.php'><i class='material-icons left'>home</i>Home</a></li>
								<li><a href='../public/Menu.php'><i class='material-icons left'>receipt</i>Menu</a></li>
							    <li><a href='../public/Sucursales.php'><i class='material-icons left'>business</i>Sucursales</a></li>
								<li><a href='../public/Nosotros.php'><i class='material-icons left'>group</i>Nosotros</a></li>
							<li><a href='../public/Iniciar.php'><i class='material-icons left'>explicit</i>Inicicar Session</a></li>
							</ul>
							<ul id='dropdown' class='dropdown-content'>
								<li><a href='../main/profile.php'><i class='material-icons left'>edit</i>Editar perfil</a></li>
								<li><a href='../main/logout.php'><i class='material-icons left'>clear</i>Salir</a></li>
							</ul>
						</div>
					</nav>
				</header>
				<main >
			");
		}
	}

	public static function footer()
	{
		print("
			</main>
			<footer class='page-footer green'>
				<div class='container'>
				
				</div>
				<div class='footer-copyright'>
					<div class='container'>
						<span>©".date(' Y ')."Los parados, todos los derechos reservados.</span>
						<span class='white-text right'>Diseñado con <a class='red-text text-accent-1' href='http://materializecss.com/' target='_blank'><b>Materialize</b></a></span>
					</div>
				</div>
			</footer>
			<script src='../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/inicializacion.js'></script>
			<script type='text/javascript' src='../js/ohsnap.js'></script>
			<script type='text/javascript' src='../js/dashboard.js'></script>
			<script type='text/javascript' src='../js/intro.js'></script>
			</body>
			</html>
		");
	}
	public static function setCombo($label, $name, $value, $query)
	{
		$data = Database::getRows($query, null);
		print("<select name='$name' required>");
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}



	public static function showMessage($type, $message, $url)
	{
		$text = addslashes($message);
		switch($type)
		{
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
				$title = "Advertencia";
				$icon = "warning";
				break;
			case 4:
				$title = "Aviso";
				$icon = "info";
			case 5:
				$title = "Bienvenido" ;
				$icon = "success";
		}
		if($url != null)
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>