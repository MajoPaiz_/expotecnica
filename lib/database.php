<?php
class Database
{
    private static $connection;
    private static $statement;
    public static $id;
    public static $error;
//funcion para conectar la base de datos 
    private static function connect()
    {
        $server = "localhost";
        $database = "losparados";
        $username = "root";
        $password = "";
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names utf8");     
        try
        {
            @self::$connection = new PDO("mysql:host=".$server."; dbname=".$database, $username, $password, $options);
        }
        catch(PDOException $exception)
        {
            throw new Exception($exception->getCode());
        }
    }
// funcion para desconectar la base de datos 
    private static function desconnect()
    {
        self::$error = self::$statement->errorInfo();
        self::$connection = null;
    }
// funcion para ejecutar las filas 
    public static function executeRow($query, $values)
    {
        self::connect();
        self::$statement = self::$connection->prepare($query);
        $state = self::$statement->execute($values);
        self::$id = self::$connection->lastInsertId();
        self::desconnect();
        return $state;
    }
// funcion para tomar los datos de las filas 
    public static function getRow($query, $values)
    {
        self::connect();
        self::$statement = self::$connection->prepare($query);
        self::$statement->execute($values);
        self::desconnect();
        return self::$statement->fetch();
    }
//funcion para atrapar los datos 
    public static function getRows($query, $values)
    {
        self::connect();
        self::$statement = self::$connection->prepare($query);
        self::$statement->execute($values);
        self::desconnect();
        return self::$statement->fetchAll();
    }
}
?>