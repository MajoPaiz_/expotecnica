<?php
require("../lib/page.php");
Page::header("Sucursales");
// se realiza la paginacion 
    $sql2 = "SELECT count(*) AS cantidad FROM sucursales, usuarios WHERE sucursales.codigo_usuario = usuarios.codigo_usuario ORDER BY nombre_sucursal";
	$data2 = Database::getRow($sql2, null);
	$Cantidad =$data2["cantidad"];
	$properpag=4;
	@$norpag=$_GET['num'];
	if($norpag==null)
	{
		$norpag=1;
	}
	$canpag=$Cantidad/$properpag;
	if($Cantidad%$properpag!=0)
	{
		$canpag=$canpag+1;
	}
	if(is_numeric($norpag))
	{
			$inicio=($norpag-1)*$properpag;
	}
	else
	{
		$inicio=0;
	}
// se incicia e buscador 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM sucursales, usuarios WHERE sucursales.codigo_usuario = usuarios.codigo_usuario AND alias LIKE ? ORDER BY alias";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM sucursales, usuarios WHERE sucursales.codigo_usuario = usuarios.codigo_usuario ORDER BY nombre_sucursal limit $inicio,$properpag ";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- se crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>DIRECCION</th>
			<th>GERENTE</th>
			<th>IMAGEN</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
// crea la taba de sucursales 
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombre_sucursal']."</td>
				<td>".$row['direccion']."</td>
				<td>".$row['alias']."</td>
				<td><img src='data:image/*;base64,".$row['imagen_sucursal']."' class='materialboxed' width='100' height='100'></td>
				<td>
		");
		if($row['estado_sucursal'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_sucursal']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_sucursal']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
	?>
    
	<div class="row center aling">
		<ul class="pagination">
		<?php
		if($norpag>1)
		{
                echo "<li class='waves-effect'><a  href='index.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
		}
		else
		{
			  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
		}
		for($i=1;$i<=$canpag;$i++)
		{
			if($i==$norpag)
			{ 

                   echo "  <li class='active green'><a >$i</a></li>";
			}
			else
			{
                   echo "<li class='waves-effect'><a  href='index.php?num=$i'>$i</a></li>";   
			}
		}
		if($norpag<$canpag-1)
		{
                echo "<li class='waves-effect'><a  href='index.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
		}
		else
		{
			  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
		}
		?>
         </ul>
		 
		</div>
		
	<?php
	
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>