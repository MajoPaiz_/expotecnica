<?php
require("../lib/page.php");
// toma los datos para agregar una sucursal 




    if(isset($_GET['id']) && ctype_digit($_GET['id'])) 
    {
        Page::header("Modificar sucursal");
        $id = $_GET['id'];
        $sql = "SELECT * FROM sucursales WHERE codigo_sucursal = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
            if($data != null)
        {
           
           
            $nombre = $data['nombre_sucursal'];
            $direccion = $data['direccion'];
            $imagen = $data['imagen_sucursal'];
            $estado = $data['estado_sucursal'];
            $gerente = $data['codigo_usuario'];
                }
        else
        {
            header("location: index.php");
        }
    }
    else
    {
        if(empty($_GET['id']))
        {
         
            Page::header("Agregar Sucursal");
            $id = null;
            $nombre = null;
            $direccion = null;
            $imagen = null;
            $gerente = null;
                }
        else
        {
            header("location: index.php");
        }
    }
    if(!empty($_POST))
    {
        $_POST = Validator::validateForm($_POST);
        $nombre = $_POST['nombre_sucursal'];
        $direccion = $_POST['direccion'];
      $archivo = $_FILES['imagen_sucursal'];
      $estado = 1;
      $gerente = $_POST['usuarios'];
      
    
    try 
    {
        if($nombre != "")
        {
                    if($direccion != "")
                    {
                        if($gerente != "")
                        {
                            if($archivo['name'] != null)
                            {
                                $imagen = Validator::validateImage($archivo, 1500, 1500);
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
                          
                            if($id == null)
                            {
                                $sql = "INSERT INTO sucursales(nombre_sucursal, direccion, imagen_sucursal, estado_sucursal, codigo_usuario) VALUES(?, ?, ?, ?, ?)";
                                $params = array($nombre, $direccion, $imagen, $estado, $gerente);
                            }
                            else
                            {
                                $sql = "UPDATE menu SET nombre_sucursal= ?, direccion= ?, imagen_sucursal= ?, estado_sucursal= ?, codigo_usuario= ? WHERE codigo_sucursal = ?";
                                $params = array($nombre, $direccion,$imagen, $estado, $gerente, $id);
                            }
                              if(Database::executeRow($sql, $params))
                            {
                                Page::showMessage(1, "Operación satisfactoria", "index.php");
                            }
                            else
                            {
                                throw new Exception("Operación fallida");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar un gerente");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe digitar una direccion");
                    }
                }
         
        else
        {
            throw new Exception("Debe digitar el nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- se crea el formulario -->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>note_add</i>
          	<input id='nombre_sucursal' type='text' name='nombre_sucursal' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre_sucursal'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>description</i>
          	<input id='direccion' type='text' name='direccion' class='validate' value='<?php print($direccion); ?>'/>
          	<label for='direccion'>Dirección</label>
        </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT codigo_usuario, alias FROM usuarios";
            Page::setCombo("Usuarios", "usuarios", $gerente, $sql);
            ?>
        </div>
      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen_sucursal' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>
       
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>