<?php
require("../lib/page.php");
Page::header("Permisos");
 // busca un usuario por el id 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios WHERE apellidos_usuario LIKE ? OR nombres_usuario LIKE ? ORDER BY apellidos_usuario";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM usuarios ORDER BY apellidos_usuario";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
 <!-- se crea el frmulario y lo muestra  -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombres o apellidos'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped centered'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>AGREGAR</th>
			<th>ELIMINAR</th>
			<th>ACTUALIZAR</th>
			<th>CONSULTAR</th>
			<th>ACCIONES</th>
		</tr>
	</thead>
	<tbody>

<?php
//autoriza el usuario a diferentes permisos
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellidos_usuario']."</td>
				<td>".$row['nombres_usuario']."</td>
		");
		$sqlPer="SELECT * FROM permisos WHERE id_usuario=?";
		$parametros=array($row['codigo_usuario']);
		$dataper=Database::getRow($sqlPer, $parametros);
		if($dataper==null)
		{
			print("
			<td><i class='material-icons'>clear</i></td>
			<td><i class='material-icons'>clear</i></td>
			<td><i class='material-icons'>clear</i></td>
			<td><i class='material-icons'>clear</i></td>
			
			
			");
		}
		else
		{
			if($dataper['guardar']!=0)
			{
				print("
				<td><i class='material-icons'>check</i></td>
				");
			}
			else
			{
				print("
				<td><i class='material-icons'>clear</i></td>
				");
			}
			if($dataper['eliminar']!=0)
			{
				print("
				<td><i class='material-icons'>check</i></td>
				");
			}
			else
			{
				print("
				<td><i class='material-icons'>clear</i></td>
				");
			}
			if($dataper['modificar']!=0)
			{
				print("
				<td><i class='material-icons'>check</i></td>
				");
			}
			else
			{
				print("
				<td><i class='material-icons'>clear</i></td>
				");
			}
			if($dataper['seleccionar']!=0)
			{
				print("
				<td><i class='material-icons'>check</i></td>
				");
			}
			else
			{
				print("
				<td><i class='material-icons'>clear</i></td>
				");
			}
		}
		print("

		      
				<td>
					<a href='save.php?id=".$row['codigo_usuario']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_usuario']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>