<?php
require("../lib/page.php");
 // seleciona el id para registrar usuario 
if(empty($_GET['id']) && ctype_digit($_GET['id']))
{
    Page::header("Agregar usuario");
    $id = null;
    $nombres = null;
    $apellidos = null;
    $correo = null;
    $alias = null;
}
else
{
    Page::header("Modificar permisos de usuario");
    $id = $_GET['id'];
    $sql = "SELECT * FROM usuarios WHERE codigo_usuario = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_usuario'];
    $apellidos = $data['apellidos_usuario'];
    $correo = $data['email_usuario'];
    $alias = $data['alias'];
    $sqlperm="SELECT * FROM permisos WHERE id_usuario=?";
    $parametros=array($data['codigo_usuario']);
    $dataper=Database::getRow($sqlperm, $parametros);
    $guardar=$dataper['guardar'];
    $eliminar=$dataper['eliminar'];
    $modificar=$dataper['modificar'];
    $seleccionar=$dataper['seleccionar'];
    
}
 // verifica todos los datos ingresados 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_usuario'];
  	$apellidos = $_POST['apellidos_usuario'];
    $correo = $_POST['email_usuario'];
    $guardar=$_POST['guardar'];
    $eliminar=$_POST['eliminar'];
    $modificar=$_POST['modificar'];
    $seleccionar=$_POST['seleccionar'];
// agrega todos los datos 
    try 
    {
      	if($nombres != "" && $apellidos != "")
        {
            if(Validator::validateEmail($correo))
            {
                if($id == null)
                {
                    $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO usuarios(nombres_usuario, apellidos_usuario, alias, clave, email_usuario) VALUES(?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $alias, $clave, $correo);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un alias");
                    }
                }
                else
                {
                $sql = "UPDATE usuarios SET nombres_usuario= ?, apellidos_usuario= ?, email_usuario= ?, alias= ? WHERE codigo_usuario = ?";
                    $params = array($nombres, $apellidos, $correo, $alias, $id);
                }
                if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- crea el formulario -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_usuario' type='text' name='nombres_usuario' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_usuario'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_usuario' type='text' name='apellidos_usuario' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_usuario'>Apellidos</label>
        </div>
    </div>
    <div class='row'>

      <input type="checkbox" id="test" value="1" <?php print(($guardar)?"checked":""); ?> name="numero[]"/>
        <label for="test">Guardar</label>
         <input type="checkbox" id="test2" value="2" <?php print(($eliminar)?"checked":""); ?> name="numero[]"/>
        <label for="test2">Eliminar</label>

    </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>