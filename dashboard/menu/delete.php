<?php
require("../lib/page.php");
Page::header("Eliminar producto");
// selecciona el id del producto 
if(!empty($_GET['id']) && ctype_digit($_GET['id']))
{
    $id = $_GET['id'];
}
else
{
    header("location: index.php");
}
// elimina el id del producto 
if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		$sql = "DELETE FROM menu WHERE codigo_menu = ?";
	    $params = array($id);
	    if(Database::executeRow($sql, $params))
		{
			Page::showMessage(1, "Operación satisfactoria", "index.php");
		}
		else
		{
			throw new Exception("Operación fallida");
		}
	}
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<!-- crea el formulario -->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>

<?php
Page::footer();
?>