<?php
require("../lib/page.php");
Page::header("Menu");
//abre el buscar 
$sql2 = "SELECT count(*) AS cantidad  FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu ORDER BY tipo_menu ";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=6;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
} 

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu AND tipo_menu LIKE ? ORDER BY tipo_menu";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu ORDER BY tipo_menu limit $inicio,$properpag";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>NOMBRE</th>
			<th>PRECIO (US$)</th>
			<th>CATEGORÍA</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_menu']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_menu']."</td>
				<td>".$row['precio']."</td>
				<td>".$row['tipo_menu']."</td>
				<td>
		");
		if($row['estado_menu'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_menu']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_menu']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
?>




</div><!-- Fin de row -->
<div class="row center aling">
<ul class="pagination">
<?php
if($norpag>1)
{
	echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
}
for($i=1;$i<=$canpag;$i++)
{
if($i==$norpag)
{ 

	   echo "  <li class='active green'><a >$i</a></li>";
}
else
{
	   echo "<li class='waves-effect'><a  href='menu.php?num=$i'>$i</a></li>";   
}
}
if($norpag<$canpag-1)
{
	echo "<li class='waves-effect'><a  href='menu.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
}
?>
</ul>

</div>

</div><!-- Fin de container -->
<?php
Page::footer();
?>