<?php
require("../lib/page.php");
//toma el id para agregar la reservacion 

    if(isset($_GET['id']) && ctype_digit($_GET['id'])) 
    {
        Page::header("Modificar Reservación");
        $id = $_GET['id'];
        $sql = "SELECT * FROM reservaciones WHERE codigo_reservacion = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
        if($data != null)
        {
           
           
            $cliente = $data['codigo_cliente'];
            $fecha = $data['fecha_reservacion'];
            $hora = $data['hora_reservacion'];
            $estado = $data['estado_reservacion'];
            $sucursal = $data['codigo_sucursal'];
                        }
        else
        {
            header("location: index.php");
        }
    }
    else
    {
        if(empty($_GET['id']))
        {
         
            Page::header("Agregar Reservación");
            $id = null;
            $cliente = null;
            $fecha = null;
            $hora = null;
            $estado = null;
            $sucursal = null;
                        }
        else
        {
            header("location: index.php");
        }
    }
    if(!empty($_POST))
    {
        $_POST = Validator::validateForm($_POST);
        $cliente = $_POST['cliente'];
        $fecha = $_POST['fecha'];
      $hora = $_POST['hora'];
      $estado =1;
      $sucursal = $_POST['sucursal'];
  
    try 
    {
        if($cliente != "")
        {
            if($fecha != "")
            {
                    if($hora != "")
                    {
                        if($sucursal != "")
                        {
                            if($id == null)
                            {
                                $sql = "INSERT INTO reservaciones (codigo_cliente, fecha_reservacion, hora_reservacion, estado_reservacion,codigo_sucursal) VALUES(?, ?, ?, ?, ?)";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal);
                            }
                            else
                            {
                                $sql = "UPDATE reservaciones SET codigo_cliente = ?, fecha_reservacion = ?, hora_reservacion = ?, estado_reservacion = ?,codigo_sucursal = ? WHERE codigo_reservacion = ?";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal, $id);
                            }
                            if(Database::executeRow($sql, $params))
                            {
                                Page::showMessage(1, "Operación satisfactoria", "index.php");
                            }
                            else
                            {
                                throw new Exception("Operación fallida");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar una sucursal");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe digitar una hora");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar una fecha");
                }
            }
        else
        {
            throw new Exception("Debe seleccionar un cliente");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- se crea el formulario -->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <?php
            $sql = "SELECT codigo_cliente, nombres_cliente FROM clientes";
            Page::setCombo("Cliente", "cliente", $cliente, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>work</i>
            <?php
            $sql = "SELECT codigo_sucursal, nombre_sucursal FROM sucursales";
            Page::setCombo("Sucursal", "sucursal", $sucursal, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>tab_unselected</i>
          	<input id="hora" type="time" name="hora" step="1" class='validate' value='<?php print($hora); ?>' required/>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>tab</i>
          	<input id="fecha" type="date" name="fecha" step="1" class='validate' value='<?php print($fecha); ?>' required/>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>