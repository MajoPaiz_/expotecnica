
<?php
require("../lib/page.php");
Page::header("Grafico de reservaciones por cliente");
require('../fpdf/fpdf.php');
$sql = "SELECT COUNT(reservaciones.codigo_reservacion) AS cantidad, reservaciones.codigo_cliente, reservaciones.fecha_reservacion, clientes.nombres_cliente FROM reservaciones INNER JOIN clientes ON reservaciones.codigo_cliente = clientes.codigo_cliente GROUP BY reservaciones.codigo_cliente";
$data = Database::getRows($sql, null);
?>
  <head>
  
    <script type="text/javascript" src="../js/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        // Crea la tabla.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Nombre');
        data.addColumn('number', 'Codigo');
        data.addRows([
         <?php foreach($data as $row2)
        {
         print ("['".$row2['nombres_cliente']."', ".$row2['cantidad']."],");
        }
        ?>
        ]);

        // Selecciona las opciones de la tabla
        var options = {'title':'RESERVACIONES POR CLIENTE',
                       'width':900,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <a type="button"  class='btn waves-effect indigo' href = "./index.php">Volver</a>
  <body>
    <!--pie de pagina -->
    <div id="chart_div"></div>
  </body>
  <?php
  
  Page::footer();
  ?>
  
  
