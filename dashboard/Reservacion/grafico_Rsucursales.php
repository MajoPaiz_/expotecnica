
<?php
require("../lib/page.php");
Page::header("Grafico de reservaciones por Sucursal");

require('../fpdf/fpdf.php');
$sql = "SELECT COUNT(reservaciones.codigo_reservacion) AS cantidad, reservaciones.codigo_cliente, sucursales.nombre_sucursal AS sucursal FROM reservaciones INNER JOIN sucursales ON reservaciones.codigo_sucursal = sucursales.codigo_sucursal GROUP BY reservaciones.codigo_sucursal";
$data = Database::getRows($sql, null);
?>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        // Crea la tabla .
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Nombre');
        data.addColumn('number', 'Reservaciones');
        data.addRows([
         <?php foreach($data as $row2)
        {
         print ("['".$row2['sucursal']."', ".$row2['cantidad']."],");
        }
        ?>
        ]);

        // seleciona todos los datos y los muestra
        var options = {'title':'RESERVACIONES POR SUCURSAL',
                       'width':900,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <a type="button"  class='btn waves-effect indigo' href = "./index.php">Volver</a>
  <body>
    <!--pie de pagina-->
    <div id="chart_div"></div>
  </body>
  <?php
  
  Page::footer();
  ?>
