<?php
require("../lib/page.php");
Page::header("Categorías");

$sql2 = "SELECT count(*) AS cantidad  FROM tipo_menu ORDER BY tipo_menu";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=6;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
} 
//if para verificar tiempo
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
//Aqui se crea un If para crear un buscador 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM tipo_menu WHERE tipo_menu LIKE ? ORDER BY tipo_menu ";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM tipo_menu ORDER BY tipo_menu limit $inicio,$properpag";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- aqui se crea el contenedor para la busqueda y la tabla donde se muestran los valores -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>DESCRIPCIÓN</th>
			<th>ACCIÓN</th>
			<th>MENU</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['tipo_menu']."</td>
				<td>".$row['descripcion']."</td>
				<td>
					<a href='save.php?id=".$row['codigo_tipomenu']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_tipomenu']."' class='red-text'><i class='material-icons'>delete</i></a>
					<td><a href='../reportes/reporte_tipo.php?id=".$row['codigo_tipomenu']."'>ver</a></td>
					</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
?>
<div class="row center aling">
<ul class="pagination">
<?php
if($norpag>1)
{
		echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
}
else
{
	  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
}
for($i=1;$i<=$canpag;$i++)
{
	if($i==$norpag)
	{ 

		   echo "  <li class='active green'><a >$i</a></li>";
	}
	else
	{
		   echo "<li class='waves-effect'><a  href='menu.php?num=$i'>$i</a></li>";   
	}
}
if($norpag<$canpag-1)
{
		echo "<li class='waves-effect'><a  href='menu.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
}
else
{
	  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
}
?>
 </ul>
 
</div>
<?php
Page::footer();
?>