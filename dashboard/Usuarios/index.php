<?php
require("../lib/page.php");
Page::header("Usuarios");
$sql2 = "SELECT count(*) AS cantidad  FROM usuarios ORDER BY apellidos_usuario ";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=6;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
} 
//busca el usuario y lo selecciona 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios WHERE apellidos_usuario LIKE ? OR nombres_usuario LIKE ? ORDER BY apellidos_usuario";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM usuarios ORDER BY apellidos_usuario limit $inicio,$properpag";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- crea el formularo -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombres o apellidos'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>CORREO</th>
			<th>ALIAS</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellidos_usuario']."</td>
				<td>".$row['nombres_usuario']."</td>
				<td>".$row['email_usuario']."</td>
				<td>".$row['alias']."</td>
				<td>
					<a href='save.php?id=".$row['codigo_usuario']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_usuario']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
?>




</div><!-- Fin de row -->
<div class="row center aling">
<ul class="pagination">
<?php
if($norpag>1)
{
	echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
}
for($i=1;$i<=$canpag;$i++)
{
if($i==$norpag)
{ 

	   echo "  <li class='active green'><a >$i</a></li>";
}
else
{
	   echo "<li class='waves-effect'><a  href='menu.php?num=$i'>$i</a></li>";   
}
}
if($norpag<$canpag-1)
{
	echo "<li class='waves-effect'><a  href='menu.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
}
?>
</ul>

</div>

</div><!-- Fin de container -->
<?php
Page::footer();
?>