<?php
require("../lib/page.php");
Page::header("Verificar usuario");
//selecciona el usuario 
$sql = "SELECT * FROM usuarios";
$data = Database::getRows($sql, null);
if($data == null)
{
    header("location: ../main/register.php");
}
// verificar todos los datos del usuario 
if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$clave = $_POST['clave'];
  	try
    {
      	if($clave != "")
  		{
  			$sql = "SELECT * FROM usuarios WHERE codigo_usuario = ?";
		    $params = array($_SESSION['id_usuario']);
		    $data = Database::getRow($sql, $params);
		    if($data != null)
		    {
		    	$hash = $data['clave'];
		    	if(password_verify($clave, $hash)) 
		    	{
			    	$_SESSION['verifiacion_usuario'] = 1;
			       	Page::showMessage(1, "Cuenta verificada", "cambiar_contra.php");
				}
				else 
				{
					throw new Exception("La clave ingresada es incorrecta");
				}
			}
		    else
		    {
		    	throw new Exception("El usuario ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un alias y una clave");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<!-- crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='clave' type='text' name='clave' require  class='validate' required/>
	    	<label for='clave'>Clave:</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>

<?php
Page::footer();
?>