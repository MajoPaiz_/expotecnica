<?php
require("../lib/page.php");
Page::header("Editar perfil");
// selecciona todos los datos del usuario 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_usuario'];
  	$apellidos = $_POST['apellidos_usuario'];
    $correo = $_POST['email_usuario'];
    $alias = $_POST['alias'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];
// edita todos los datos del usuario 
    try 
    {
      	if($nombres != "" && $apellidos != "")
        {
            if(Validator::validateEmail($correo))
            {
                if($alias != "")
                {
                    if($clave1 != "" || $clave2 != "")
                    {
                        if($clave1 == $clave2)
                        {
                            $clave = password_hash($clave1, PASSWORD_DEFAULT);
                            $sql = "UPDATE usuarios SET nombres_usuario = ?, apellidos_usuario = ?, email_usuario = ?, alias = ?, clave = ? WHERE codigo_usuario = ?";
                            $params = array($nombres, $apellidos, $correo, $alias, $clave, $_SESSION['id_usuario']);
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE usuarios SET nombres_usuario = ?, apellidos_usuario = ?, email_usuario = ?, alias = ? WHERE codigo_usuario = ?";
                        $params = array($nombres, $apellidos, $correo, $alias, $_SESSION['id_usuario']);
                    }
                    if(Database::executeRow($sql, $params))
                    {
                        Page::showMessage(1, "Operación satisfactoria", "index.php");
                    }
                    else
                    {
                        throw new Exception("Operación fallida");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar un alias");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT * FROM usuarios WHERE codigo_usuario = ?";
    $params = array($_SESSION['id_usuario']);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_usuario'];
    $apellidos = $data['apellidos_usuario'];
    $correo = $data['email_usuario'];
    $alias = $data['alias'];
}
?>
<!-- se crea el formulario -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_usuario' type='text' name='nombres_usuario' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_usuario'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_usuario' type='text' name='apellidos_usuario' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_usuario'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_usuario' type='email' name='email_usuario' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' value='<?php print($alias); ?>' required/>
            <label for='alias'>Alias</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR CLAVE</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate'/>
            <label for='clave1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate'/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='../main/index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>