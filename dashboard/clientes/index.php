<?php
require("../lib/page.php");
Page::header("Clientes");
 $sql2 = "SELECT count(*) AS cantidad FROM clientes ORDER BY apellidos_cliente";
 $data2 = Database::getRow($sql2, null);
 $Cantidad =$data2["cantidad"];
 $properpag=6;
 @$norpag=$_GET['num'];
 if($norpag==null)
 {
	 $norpag=1;
 }
 $canpag=$Cantidad/$properpag;
 if($Cantidad%$properpag!=0)
 {
	 $canpag=$canpag+1;
 }
 if(is_numeric($norpag))
 {
		 $inicio=($norpag-1)*$properpag;
 }
 else
 {
	 $inicio=0;
 }
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM clientes WHERE apellidos_cliente LIKE ? OR nombres_cliente LIKE ? ORDER BY apellidos_cliente";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM clientes ORDER BY apellidos_cliente limit $inicio,$properpag";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- aqui se crea el contenedor para la busqueda y la tabla donde se muestran los valores -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombres o apellidos'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- crea las tablas y ponerle nombre a cda campo-->
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>ALIAS</th>
			<th>NOMBRE</th>
			<th>APELLIDOS</th>
			<th>CORREO</th>
			<th>TELEFONO</th>
			<th>RESERVACIONES</th>
			<th>EVENTOS</th>
			<th>BANEOS</th>
		</tr>
	</thead>
	<tbody>

<?php
// ordena los valore que se han omado de la base de datos 
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_perfil']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['alias']."</td>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['apellidos_cliente']."</td>
				<td>".$row['email_cliente']."</td>
				<td>".$row['telefono_cliente']."</td>
			    <td><a href='../reportes/reportes_reservaciones.php?id=".$row['codigo_cliente']."' target='_blank'>ver</a></td>
				<td><a href='../reportes/reporte_eventos.php?id=".$row['codigo_cliente']."' ' target='_blank'>ver</a></td>
				<td><a href='../reportes/reporte_baneado.php?id=".$row['codigo_cliente']."' ' target='_blank'>ver</a></td>
				
				<td>
					<a href='save.php?id=".$row['codigo_cliente']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_cliente']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
	</tbody>
</table>
");
?>

<div class="row center aling">
	<ul class="pagination">
	<?php
	if($norpag>1)
	{
			echo "<li class='waves-effect'><a  href='index.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
	}
	for($i=1;$i<=$canpag;$i++)
	{
		if($i==$norpag)
		{ 

			   echo "  <li class='active green'><a >$i</a></li>";
		}
		else
		{
			   echo "<li class='waves-effect'><a  href='index.php?num=$i'>$i</a></li>";   
		}
	}
	if($norpag<$canpag-1)
	{
			echo "<li class='waves-effect'><a  href='index.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
	}
	?>
	 </ul>
	 
	</div>
	
<?php

} //Fin de if que comprueba la existencia de registros.
else
{
Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>