<?php
require("../lib/page.php");
//verifica el id , si no se encuentra te manda al formulario de guardar 

    if(isset($_GET['id']) && ctype_digit($_GET['id'])) 
    {
        Page::header("Modificar cliente");
        $id = $_GET['id'];
        $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
                if($data != null)
        {
           
           
            $nombres = $data['nombres_cliente'];
            $apellidos = $data['apellidos_cliente'];
            $alias = $data['alias'];
            $imagen = $data['imagen_perfil'];
            $correo = $data['email_cliente'];
            $telefono = $data['telefono_cliente'];
                        }
        else
        {
            header("location: index.php");
        }
    }
    else
    {
        if(empty($_GET['id']))
        {
         
            Page::header("Agregar cliente");
            $id = null;
            $nombres = null;
            $apellidos = null;
            $alias = null;
            $imagen = null;
            $correo= null;
            $telefono = null;
                        }
        else
        {
            header("location: index.php");
        }
    }
    if(!empty($_POST))
    {
 
        $_POST = Validator::validateForm($_POST);
        $nombres = $_POST['nombres_cliente'];
      $apellidos = $_POST['apellidos_cliente'];
      $archivo = $_FILES['imagen'];
        $correo = $_POST['email_cliente'];
      $telefono = $_POST['telefono_cliente'];
          
    try 
    {
        if($archivo['name'] != null)
                            {
                                $imagen = Validator::validateImage($archivo, 1200, 1200);
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
         	if($nombres != "" && $apellidos != "")
        {
            if(Validator::validateEmail($correo))
            {
                if($id == null)
                {
                    $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO clientes (nombres_cliente, apellidos_cliente, alias, imagen_perfil, clave, email_cliente, telefono_cliente) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $alias,$imagen, $clave, $correo,$telefono);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un alias");
                    }
                }
                else
                {
                $sql = "UPDATE clientes SET nombres_cliente= ?, apellidos_cliente= ?, alias= ?, imagen_perfil= ?,email_cliente= ?,telefono_cliente= ? WHERE codigo_cliente = ?";
                    $params = array($nombres, $apellidos, $alias,$imagen, $correo,$telefono, $id);
                }
                if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico válido");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }


    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<script type="application/x-javascript">
function validar(formulario){
      if (formulario.dato.value.length != 8)
           { alert("Debe introducir una contraseña de 10 dígitos")}
}
</script> 
                            

<!-- se crea el formulario -->
<form method='post' enctype='multipart/form-data'>
<div class='row'>
        
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_cliente' type='text' name='nombres_cliente' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_cliente' type='text' name='apellidos_cliente' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_cliente'>Apellidos</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_cliente' type='email' name='email_cliente' class='validate' value='<?php print($correo); ?>' required/>
            <label for='email_cliente'>Correo</label>
        </div>
        
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>
        
        <div class='input-field col s12 m6  ' method=post onSubmit="return validar(this)" >
          	<i class='material-icons prefix'>call</i>
          	<input id='telefono_cliente' type='text' maxlength="11" size="11" name='telefono_cliente' class='validate'  value='<?php print($telefono); ?>' required/>
          	<label for='telefono_cliente'>Telefono</label>
        </div>

      	<div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
        
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>

    </div>
    <?php
    //verifica la cave y la contraseña 
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>