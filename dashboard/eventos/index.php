<?php
require("../lib/page.php");
Page::header("Eventos");
$sql2 = "SELECT count(*) AS cantidad   FROM eventos e, clientes c, sucursales s WHERE e.codigo_cliente = c.codigo_cliente AND e.codigo_sucursal = s.codigo_sucursal ORDER BY nombres_cliente ";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=6;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
} 
?>

<?php
// selecciona el id de eventos 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM eventos e, clientes c, sucursales s WHERE e.codigo_cliente = c.codigo_cliente AND e.codigo_sucursal = s.codigo_sucursal LIKE ? ORDER BY nombres_cliente";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM eventos e, clientes c, sucursales s WHERE e.codigo_cliente = c.codigo_cliente AND e.codigo_sucursal = s.codigo_sucursal ORDER BY nombres_cliente limit $inicio,$properpag";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>


<!-- se crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>FECHA</th>
			<th>DIRECCIÓN</th>
			<th>SUCURSAL</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

	<a type="button" class='btn waves-effect indigo' href = "./grafico_eventos.php">Grafico de Eventos por cliente</a>
<a type="button"  class='btn waves-effect indigo' href = "./grafico_Esucursales.php">Grafico de Eventos por Sucursal</a>



<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['fecha_evento']."</td>
				<td>".$row['direccion_evento']."</td>
				<td>".$row['nombre_sucursal']."</td>
				<td>
		");
		if($row['estado_evento'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_evento']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_evento']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
		
	</table>
	
	");
} //Fin de if que comprueba la existencia de registros.		else
else
{
	print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
}
?>




</div><!-- Fin de row -->
<div class="row center aling">
<ul class="pagination">
<?php
if($norpag>1)
{
	echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
}
for($i=1;$i<=$canpag;$i++)
{
if($i==$norpag)
{ 

	   echo "  <li class='active green'><a >$i</a></li>";
}
else
{
	   echo "<li class='waves-effect'><a  href='menu.php?num=$i'>$i</a></li>";   
}
}
if($norpag<$canpag-1)
{
	echo "<li class='waves-effect'><a  href='menu.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
}
else
{
  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
}
?>
</ul>

</div>

</div><!-- Fin de container -->
<?php
Page::footer();
?>