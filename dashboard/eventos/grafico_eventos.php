
<?php
require("../lib/page.php");
Page::header("Grafico de Eventos por cliente");
$sql = "SELECT COUNT(eventos.codigo_evento) AS cantidad, eventos.codigo_cliente, eventos.fecha_evento, clientes.nombres_cliente AS nombre FROM eventos INNER JOIN clientes ON eventos.codigo_cliente = clientes.codigo_cliente GROUP BY eventos.codigo_cliente";
$data = Database::getRows($sql, null);
?>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Nombre');
        data.addColumn('number', 'Cantidad');
        data.addRows([
         <?php foreach($data as $row2)
        {
         print ("['".$row2['nombre']."', ".$row2['cantidad']."],");
        }
        ?>
        ]);

        // Set chart options
        var options = {'title':'EVENTOS POR CLIENTE',
                       'width':900,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <a type="button"  class='btn waves-effect indigo' href = "./index.php">Volver</a>
  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>

  <?php

Page::footer();
?>

