<?php
require("../lib/page.php");
// seleccina el id para agregar evento 





    if(isset($_GET['id']) && ctype_digit($_GET['id'])) 
    {
        Page::header("Modificar Evento");
        $id = $_GET['id'];
        $sql = "SELECT * FROM eventos WHERE codigo_evento = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
         if($data != null)
        {
           
            $cliente = $data['codigo_cliente'];
            $fecha = $data['fecha_evento'];
            $direccion = $data['direccion_evento'];
            $estado = $data['estado_evento'];
            $sucursal = $data['codigo_sucursal'];
            }
        else
        {
            header("location: index.php");
        }
    }
    else
    {
        if(empty($_GET['id']))
        {
         
     
    Page::header("Agregar Evento");
    $id = null;
    $cliente = null;
    $fecha = null;
    $direccion = null;
    $estado = null;
    $sucursal = null;
           }
        else
        {
            header("location: index.php");
        }
    }
    if(!empty($_POST))
    {
 
        $_POST = Validator::validateForm($_POST);
        $cliente = $_POST['cliente'];
        $fecha = $_POST['fecha'];
      $direccion = $_POST['direccion'];
      $estado =1;
      $sucursal = $_POST['sucursal'];
       
    try 
    {
        if($cliente != "")
        {
            if($fecha != "")
            {
                if($direccion != "")
                {
                    if($sucursal != "")
                    {
                        if($id == null)
                        {
                            $sql = "INSERT INTO eventos (direccion_evento, fecha_evento, estado_evento, codigo_cliente, codigo_sucursal) VALUES(?, ?, ?, ?, ?)";
                            $params = array($direccion, $fecha, $estado, $cliente, $sucursal);
                        }
                        else
                        {
                            $sql = "UPDATE eventos SET direccion_evento = ?, fecha_evento = ?, estado_evento = ?, codigo_cliente = ?, codigo_sucursal = ? WHERE codigo_evento = ?";
                            $params = array($direccion, $fecha, $estado, $cliente, $sucursal, $id);
                        }
                        if(Database::executeRow($sql, $params))
                        {
                            Page::showMessage(1, "Operación satisfactoria", "index.php");
                        }
                        else
                        {
                            throw new Exception("Operación fallida");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe seleccionar una sucursal");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar una direccion");
                }
            }
            else
            {
                throw new Exception("Debe ingresar una fecha");
            }
        }
    else
    {
        throw new Exception("Debe seleccionar un cliente");
    }
}
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <?php
            $sql = "SELECT codigo_cliente, nombres_cliente FROM clientes";
            Page::setCombo("Cliente", "cliente", $cliente, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>work</i>
            <?php
            $sql = "SELECT codigo_sucursal, nombre_sucursal FROM sucursales";
            Page::setCombo("Sucursal", "sucursal", $sucursal, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>picture_in_picture</i>
          	<input id="fecha" type="date" name="fecha" step="1" class='validate' value='<?php print($fecha); ?>' required/>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>location_on</i>
            <input id='direccion' type='text' name='direccion' class='validate' value='<?php print($direccion); ?>' required/>
            <label for='direccion'>Direccion</label>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i></label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>