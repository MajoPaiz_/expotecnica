<?php
require("../lib/page.php");
Page::header("Reservaciones");


?>

<?php include("../inc/social.php");?>

<!-- se capturan los datos de la reservacion -->

<?php
if(empty($_GET['id'])) 
{
    
    $id = null;
    $cliente = null;
    $fecha = null;
    $hora = null;
    $estado = null;
    $sucursal = null;
}
else
{
   
   $id = $_GET['id'];
    $sql = "SELECT * FROM reservaciones WHERE codigo_reservacion = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $cliente = $data['codigo_cliente'];
    $fecha = $data['fecha_reservacion'];
    $hora = $data['hora_reservacion'];
    $estado = $data['estado_reservacion'];
    $sucursal = $data['codigo_sucursal'];
}
// se establece las variable para guardar en a base de datos 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
    
  	$cliente = $_POST['cliente'];
  	$fecha = $_POST['fecha'];
    $hora = $_POST['hora'];
    $estado = 1;
    $sucursal = $_POST['sucursal'];

    try 
    {
        if($cliente != "")
        {
            if($fecha != "")
            {
                    if($hora != "")
                    {
                        if($sucursal != "")
                        {
                            if($id == null)
                            {

                                
                            


                                $sql = "INSERT INTO reservaciones (codigo_cliente, fecha_reservacion, hora_reservacion, estado_reservacion,codigo_sucursal) VALUES(?, ?, ?, ?, ?)";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal);
                            }
                            else
                            {
                                $sql = "UPDATE reservaciones SET codigo_cliente = ?, fecha_reservacion = ?, hora_reservacion = ?, estado_reservacion = ?,codigo_sucursal = ? WHERE codigo_reservacion = ?";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal, $id);
                            }
                            if(Database::executeRow($sql, $params))
                            {
                                Page::showMessage(1, "Reservacion completada ", "../main/reservaciones.php");
                            }
                            else
                            {
                                throw new Exception("Operación fallida");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar una sucursal");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe digitar una hora");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar una fecha");
                }
            }
        else
        {
            throw new Exception("Debe seleccionar un cliente");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}

?>

<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!-- or from surge.sh -->
<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!--parallax-->
<div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  <div class="container">
<form method='post' enctype='multipart/form-data'>

<div class='row'>
        <div class='input-field col s12 m6' data-step="1"  data-intro="No te preocupes por tu nombre ya lo tenemos" data-position='right' data-scrollTo='tooltip'>
          	<i class='material-icons prefix'>person</i>
              <?php
                  $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
                 $params = array($_SESSION['codigo_cliente']);
                 $data = Database::getRow($sql, $params);
                 $nombres = $data['nombres_cliente'];
                 $cliente = $data['codigo_cliente'];
                 

              ?>
 	        <input id='cliente' type='text' name='cliente' autocomplete="off" class='validate hide' value='<?php print($cliente); ?>' required readonly/>
          
          	<input id='nombres_cliente' type='text' name='nombres_cliente' autocomplete="off" class='validate' value='<?php print($nombres); ?>' required readonly/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
     <!-- se crea el formulario -->
 
         <div class='input-field col s12 m6' data-step="2" data-intro="selecciona la sucursal donde quieres la resevacion" data-position='right' data-scrollTo='tooltip'>
            <i class='material-icons prefix'>work</i>
            <?php
            $sql = "SELECT codigo_sucursal, nombre_sucursal FROM sucursales";
            Page::setCombo("Sucursal", "sucursal", $sucursal, $sql);
            ?>
        </div>
        
        <div class='input-field col s12 m6' data-step="3" data-intro="Coloca la hara que se hara tu reservacion" data-position='right' data-scrollTo='tooltip'>
          	<i class='material-icons prefix'>tab_unselected</i>
          	<input id="hora" type="time"autocomplete="off" name="hora" step="1" class='validate' value='<?php print($hora); ?>' required/>
        </div>
        
        <div class='input-field col s12 m6' data-step="4" data-intro="Coloca la fecha que se hara tu reservacion" data-position='right' data-scrollTo='tooltip'>
          	<i class='material-icons prefix'>tab</i>
          	<input id="fecha" type="date"  autocomplete="off" name="fecha" step="1" class='validate' value='<?php print($fecha); ?>' required/>
        </div>
        
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue' data-step="5" data-intro="Guarda los datos " data-position='right' data-scrollTo='tooltip'><i class='material-icons'>save</i></button>
        <a class="waves-effect waves-light btn right shake-slow shake-constant shake-constant--hover" href="javascript:void(0);" onclick="javascript:introJs().setOption('showProgress', true).start();">Ayuda</a>
 
    </div>
    </div>
</form>

<?php
Page::footer();
?>
