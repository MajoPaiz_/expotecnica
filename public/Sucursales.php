<?php
require("../lib/page.php");
Page::header("sucursales");?>

 <!-- Sección de productos -->

 <div id="index-banner" class="parallax-container">
 <div class="section no-pad-bot">
 <div class="container   pulse">
   <br><br>
   <br><br>  <br><br>
	 <div class="row center ">
	 <h1 class=" col s12 m12 green accent-3 white-text">Sucursales</h1>
   </div>
   <br><br>

 </div>
</div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>
	<div class='container' id='productos'>
		
		<div class='row'>
		<?php
	
	
			$sql2 = "SELECT count(*) AS cantidad  FROM sucursales ";
			$data2 = Database::getRow($sql2, null);
			$Cantidad =$data2["cantidad"];
			$properpag=6;
			@$norpag=$_GET['num'];
			if($norpag==null)
			{
				$norpag=1;
			}
			$canpag=$Cantidad/$properpag;
			if($Cantidad%$properpag!=0)
			{
				$canpag=$canpag+1;
			}
			if(is_numeric($norpag))
			{
					$inicio=($norpag-1)*$properpag;
			}
			else
			{
				$inicio=0;
			} 
			
		$sql = "SELECT * FROM sucursales limit $inicio,$properpag";
		$data = Database::getRows($sql, null);
		if($data != null)
		{
			//se crea la trjeta con el contenido de la sucursal
			foreach ($data as $row) 
			{
				print("
				<div class='card hoverable col s12 m6 l4'>
						<div class='card-image waves-effect waves-block waves-light'>
							<img class='activator' src='data:image/*;base64,$row[imagen_sucursal] ' width='300' height='250'>
						</div>
						<div class='card-content'>
							<span class='card-title activator grey-text text-darken-4  ' >$row[nombre_sucursal]<i class='material-icons right'>more_vert</i></span>
						</div>
						<div class='card-reveal'>
							<span class='card-title grey-text text-darken-4'>$row[nombre_sucursal]<i class='material-icons right'>close</i></span>
							<p>$row[direccion]</p>
						</div>
					</div>
				");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
		}
		?>
		



		</div><!-- Fin de row -->
		<div class="row center aling">
	<ul class="pagination">
	<?php
	if($norpag>1)
	{
			echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
	}
	for($i=1;$i<=$canpag;$i++)
	{
		if($i==$norpag)
		{ 

			   echo "  <li class='active green'><a >$i</a></li>";
		}
		else
		{
			   echo "<li class='waves-effect'><a  href='menu.php?num=$i'>$i</a></li>";   
		}
	}
	if($norpag<$canpag-1)
	{
			echo "<li class='waves-effect'><a  href='menu.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
	}
	?>
	 </ul>
	 
	</div>

	</div><!-- Fin de container -->
<?php
Page::footer();
?>