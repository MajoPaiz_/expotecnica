<?php
require("../lib/page.php");
Page::header("");?>

<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!-- or from surge.sh -->
<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!--parallax-->
 
<?php include("../inc/social.php");?>
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container   pulse">
        <br><br>
        <br><br>  <br><br>
          <div class="row center ">
          <h1 class=" col s12 m12 green accent-3 white-text">Historia</h1>
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>
  <div class="container">
<div class="card-panel valign-wrapper col s12 m6">
      <span class="teal-text text-darken-4  valign">Fundada en 1965 TAQUERÍA LOS PARADOS ha sido pionera en servir los mejores tacos al pastor, que por casi 50 años nos avalan.

En ese entonces sin mesas ni sillas, cautivamos a nuestros clientes hasta llegar a lo que hoy somos, una gran cadena de restaurantes mexicanos en donde siempre encontrarás la mejor calidad de productos y servicios, buscando la satisfacción de nuestros consumidores en un precio accesible y con el mejor servicio. 

Vendido más de un millón de tacos al año, TAQUERÍA LOS PARADOS tienen la filosofía de tener los mejores estándares de calidad para conservar la confianza que ya generamos y crecer nuestro mercado.



</span>

</div>
<div class="card-panel valign-wrapper col s12 m6">
<span class="teal-text text-darken-4  valign">

En TAQUERÍA LOS PARADOS sabemos que los Tacos es uno de los platillos tradicionales en la comida de las familias mexicanas, a todo mexicano le gusta tener en su mesa una tortilla caliente para iniciar los alimentos; en cualquier momento del día una tortilla es excelente alimento.

Las tortillas de maíz son especialmente importantes en la gastronomía mexicana, con ellas se preparan los tacos, tacos dorados, flautas, quesadillas, enchiladas, chilaquiles, totopos, etc. Las tortillas se comen calientes, siempre envolviendo otro alimento, como carnes, huevo, y diversas comidas.

Los tacos suelen ir acompañados de alguna salsa. 
</span>

</div>
<div class="card-panel valign-wrapper col s12 m6">
<span class="teal-text text-darken-4  valign">
El taco, como cualquier otra manifestación de cultura culinaria de México, está directamente asociado a los ingredientes utilizados en cada región geográfica del país.

TAQUERÍA LOS PARADOS es una empresa con más de 45 años de experiencia haciendo tacos a tu servicio, lo que nos permite garantizarte servicio y calidad en todos nuestros servicios.

Especialistas en tacos desde 1965

</span>

    </div>
    </div>

    <div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  


<center>
<img src="../img/LOGO3.jpg"  width=250  data-step="1" data-intro="Todo empezo en 1994 cuando decidimos abrir la primera puerta y este logo nos acompaño por mucho tiempo" data-position='right' data-scrollTo='tooltip'>
<img src="../img/logo2.png" width=200 data-step="2" data-intro="Pasaron los años y teniamos que cambiar y que mejor forma de representar una taqueria que con un trompo" data-position='right' data-scrollTo='tooltip'>
<img src="../img/tacos.png" width=250 data-step="3" data-intro="Tomando de base el trompo decidimos crear un logo. con los diseñadores llegamos a este logo que hasta hace unos años hemos decidido cambiar" data-position='right' data-scrollTo='tooltip'>
<img src="../img/menu.png" width=250 data-step="4" data-intro="Hoy en dia estamos dandonos a la tarea de cambiar a este nuevo logo, un loco nuevo y freco " data-position='left' data-scrollTo='tooltip'> 
</center>
<div class="center"> 
<a class="waves-effect waves-light btn center green shake-little shake-constant shake-constant--hover " href="javascript:void(0);" onclick="javascript:introJs().setOption('showProgress', true).start();">Nuestra Historia</a>
</div>

<div class="container">

<h1 class="center">LOS REYES DEL HOGAR</h1>
<ul class="collapsible popout" data-collapsible="accordion">
        
    <li>
      <div class="collapsible-header active"><i class="material-icons">book</i>Cuentos</div>
      <div class="collapsible-body  center"><span  >
      
      <div class="card-panel valign-wrapper col s12 m6 ">
<span class="teal-text text-darken-4  valign">
Descarga nuestros cuentos</span>

    </div>
      <a class="waves-effect waves-light btn pulse green left" href="../img/blancanieves.pdf" target="_blank">Blaca nieves</a>
      <a class="waves-effect waves-light btn pulse green " href="../img/caperucitaroja.pdf" target="_blank">La sirenita</a>
      <a class="waves-effect waves-light btn pulse green right" href="../img/lasirenita.pdf" target="_blank">Caperucita</a>
      </span>
      </div>
    </li>
    <li>
      <div class="collapsible-header active"><i class="material-icons">border_color</i>Colorea</div>
      <div class="collapsible-body  center"><span  >
      <a class="waves-effect waves-light btn pulse green left" href="../img/blancanieves.jpg" target="_blank">Blaca nieves</a>
      <a class="waves-effect waves-light btn pulse green" href="../img/sirenita.jpg" target="_blank">La sirenita</a>
      <a class="waves-effect waves-light btn pulse green right " href="../img/caperucita.jpg" target="_blank">Caperucita</a>
      </span>
      </div>  </li>
    
  </ul>

</div>
<?php
Page::footer();
?>