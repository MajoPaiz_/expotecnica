<?php
require("../lib/page.php");



if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
//if para agregar un evento 
if(empty($_GET['id'])) 
{
    Page::header("Agregar Evento");
    $id = null;
    $cliente = null;
    $fecha = null;
    $direccion = null;
    $estado = null;
    $sucursal = null;
}
else
{
    Page::header("Modificar Evento");
    $id = $_GET['id'];
    $sql = "SELECT * FROM eventos WHERE codigo_evento = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $cliente = $data['codigo_cliente'];
    $fecha = $data['fecha_evento'];
    $direccion = $data['direccion_evento'];
    $estado = $data['estado_evento'];
    $sucursal = $data['codigo_sucursal'];
}
//if para registrar un evento 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$cliente = $_POST['cliente'];
  	$fecha = $_POST['fecha'];
    $direccion = $_POST['direccion'];
    $estado = $_POST['estado'];
    $sucursal = $_POST['sucursal'];

    try 
    {
        if($cliente != "")
        {
            if($fecha != "")
            {
                if($direccion != "")
                {
                    if($sucursal != "")
                    {
                        if($id == null)
                        {
                            $sql = "INSERT INTO eventos (direccion_evento, fecha_evento, estado_evento, codigo_cliente, codigo_sucursal) VALUES(?, ?, ?, ?, ?)";
                            $params = array($direccion, $fecha, '1', $cliente, $sucursal);
                        }
                        else
                        {
                            $sql = "UPDATE eventos SET direccion_evento = ?, fecha_evento = ?, estado_evento = ?, codigo_cliente = ?, codigo_sucursal = ? WHERE codigo_evento = ?";
                            $params = array($direccion, $fecha, $estado, $cliente, $sucursal, $id);
                        }
                        if(Database::executeRow($sql, $params))
                        {
                            Page::showMessage(1, "Evento registrado", "../main/Evento.php");
                        }
                        else
                        {
                            throw new Exception("Operación fallida");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe seleccionar una sucursal");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar una direccion");
                }
            }
            else
            {
                throw new Exception("Debe ingresar una fecha");
            }
        }
    else
    {
        throw new Exception("Debe seleccionar un cliente");
    }
}
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<script type="text/javascript" src="js/noty/packaged/jquery.noty.packaged.min.js"></script>


<!-- JavaScript -->
<script src="../js/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="../css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="../css/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="../css/semantic.min.css"/>
<!-- Bootstrap theme -->

<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!-- or from surge.sh -->
<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!--parallax-->


<div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>

  <div class="container">
<form method='post' enctype='multipart/form-data'>
    
	<!-- aqui se crea el formulario -->
<div class='row'>
        <div class='input-field col s12 m6' >
          	<i class='material-icons prefix'>person</i>
              <?php
                  $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
                 $params = array($_SESSION['codigo_cliente']);
                 $data = Database::getRow($sql, $params);
                 $nombres = $data['nombres_cliente'];
                 $cliente = $data['codigo_cliente'];
                 

              ?>
 	        <input id='cliente' type='text' name='cliente' class='validate hide' value='<?php print($cliente); ?>' required readonly/>
          
          	<input id='nombres_cliente' type='text' name='nombres_cliente' class='validate' value='<?php print($nombres); ?>' required readonly/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
	
	
	
	
	
	        <div class='input-field col s12 m6' data-step="1" data-intro="Selecciona la sucursal donde quieres que sea tu evento" data-position='right' data-scrollTo='tooltip'>
            <i class='material-icons prefix'>work</i>
            <?php
            $sql = "SELECT codigo_sucursal, nombre_sucursal FROM sucursales";
            Page::setCombo("Sucursal", "sucursal", $sucursal, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'data-step="2" data-intro="Coloca la fecha que se hara tu evento" data-position='right' data-scrollTo='tooltip'>
          	<i class='material-icons prefix'>picture_in_picture</i>
          	<input id="fecha" type="date" name="fecha" step="1" class='validate ' value='<?php print($fecha); ?>' required/>
        </div>
        <div class='input-field col s12 m6' data-step="3" data-intro="Escribe la direccion de tu evento" data-position='right' data-scrollTo='tooltip'>
            <i class='material-icons prefix'>location_on</i>
            <input id='direccion' type='text' name='direccion' class='validate' value='<?php print($direccion); ?>' required/>
            <label for='direccion'>Direccion</label>
        </div>
     
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'data-step="4" data-intro="Guarda los datos" data-position='right' data-scrollTo='tooltip'><i class='material-icons'>save</i></button>
    
        <a class="waves-effect waves-light btn right shake-slow shake-constant shake-constant--hover" href="javascript:void(0);" onclick="javascript:introJs().setOption('showProgress', true).start();">Ayuda</a>
 
    </div>    

</div>

</form>
<?php
Page::footer();
?>