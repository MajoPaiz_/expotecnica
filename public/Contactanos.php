<?php
//archivo aestro que incluye el page 
require("../lib/page.php");
Page::header("Comentario");

$sql2 = "SELECT count(*) AS cantidad  FROM comentarios , clientes WHERE comentarios.codigo_cliente = clientes.codigo_cliente ORDER BY fecha_comentario";
$data2 = Database::getRow($sql2, null);
$Cantidad =$data2["cantidad"];
$properpag=5;
@$norpag=$_GET['num'];
if($norpag==null)
{
	$norpag=1;
}
$canpag=$Cantidad/$properpag;
if($Cantidad%$properpag!=0)
{
	$canpag=$canpag+1;
}
if(is_numeric($norpag))
{
		$inicio=($norpag-1)*$properpag;
}
else
{
	$inicio=0;
}


if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$comentario = $_POST['textarea1'];
    
    ini_set("date.timezone","America/El_Salvador");
    $horis= date("G:i");
    $fecha = date('Y-m-d');
    $user = $_SESSION['codigo_cliente'];

    try 
    {

          if($comentario!=null)
          {
             
              $sql = "INSERT INTO comentarios(comentario, fecha_comentario,codigo_cliente) VALUES(?, ?, ?)";
              $params = array($comentario, $fecha, $user);

              if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "Contactanos.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
          }
          else
          {
              throw new Exception("Ingrese un comntario");
          }
      
      
  


    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}

?> 
<div class="container">

<div class="row">
    <form method='post' class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" required name="textarea1"class="materialize-textarea"></textarea>
          <label for="textarea1">Comentarios</label>

        </div>
      </div>
      <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
  </button>
    </form>
  </div>

</div>
<div class='container' id='productos'>
		<h4 class='center-align'>Comentarios</h4>
		<div class='row'>
		<?php
		
		$sql = "SELECT * FROM comentarios , clientes WHERE comentarios.codigo_cliente = clientes.codigo_cliente ORDER BY fecha_comentario limit $inicio,$properpag " ;
		$data = Database::getRows($sql, null);
		if($data != null)
		{
			foreach ($data as $row) 
			{
				print("
                <ul class='collection'>
                <li class='collection-item avatar'>
                  <img src='data:image/*;base64,$row[imagen_perfil]'  class='circle'>
                  <span class='title'>$row[nombres_cliente]</span>
                  <p>Comentario <br>
                  $row[comentario]
                  </p>
                  <a href='' class='secondary-content'><i class='material-icons'>verified_user</i></a>

                  </li>
                </ul>				");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
		}
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->


    <div class="row center aling">
	<ul class="pagination">
	<?php
	if($norpag>1)
	{
			echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag-1)."'><i class='material-icons'>chevron_left</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
	}
	for($i=1;$i<=$canpag;$i++)
	{
		if($i==$norpag)
		{ 

			   echo "  <li class='active green'><a >$i</a></li>";
		}
		else
		{
			   echo "<li class='waves-effect'><a  href='ingreso.php?num=$i'>$i</a></li>";   
		}
	}
	if($norpag<$canpag-1)
	{
			echo "<li class='waves-effect'><a  href='ingreso.php?num=".($norpag+1)."'><i class='material-icons'>chevron_right</i></a></li>";
	}
	else
	{
		  echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li>";
	}
	?>
	 </ul>
	 
	</div>

<?php
Page::footer();
?>