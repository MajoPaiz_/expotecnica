<?php
require("../lib/page.php");
Page::header("Bienvenido");?>


<!-- JavaScript -->
<script src="../js/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="../css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="../css/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="../css/semantic.min.css"/>
<!-- Bootstrap theme -->

<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">
<!-- or from surge.sh -->
<link rel="stylesheet" type="text/css" href="../css/csshake.min.css">




<link href="../css/introjs.css" rel="stylesheet">
<!--parallax-->


<?php
?>
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <div class="row center">
        <h1 class="header center "><img  class="shake-slow simonlimos"src="../img/menu.png" width="500" height="300"></h1>  
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>
  <?php include("../inc/social.php");?>


  <div class="container">
    <div class="section">

      <!--   seccion  de comentario donde salen los comentarios de la empresa  -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
             <h5 class="center brown-text">Vision</h5>
             <p class="light">en poco tiempo ganarnos el cariño de nuestros clientes</p>
             </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block" >
        
            <h5 class="center brown-text" >¿Quienes somos?</h5>
            <p class="light">Somos un restaurante de El Salvador con comida mexicana que servimos Tacos, Tortas y Burritos. </p>
        
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            
            <h5 class="center brown-text">Mision</h5>

            <p class="light">Ser una gran taqueria con el mejor sabor mexicano.</p>
          </div>
        </div>
      </div>

    </div>
  </div>

<div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  <!--INFORMACION SEGUN LA NECESIDAD DE LA EMPRESA-->
  <div  class="" >           

  <div id="wheelDiv" ></div>           
  </div>    
  <div class="row">
        <div class="col s12 m6">
          <div class="card green">
            <div class="card-content white-text">
              <span class="card-title center">Productos</span>
              <p>Tenemos los mejores productos con los ingredientes mas frescos para poderte servir las mejores tortas y tacos de todo el pais,
              si quiere probar el mejor sabor mexicano buscanos en nuestras diferentes sucursales.</p>
            </div>
            
         
            <div class="card-tabs">
      <ul class="tabs tabs-fixed-width tabs-transparent white" >
        <li class="tab "><a class="green-text text-darken-2  active" href="#test4">Calidad</a></li>
        <li class="tab"><a  class="green-text text-darken-2" href="#test5">Frescura</a></li>
        <li class="tab"><a   class="green-text text-darken-2"href="#test6">Para Todos</a></li>
      </ul>
    </div>
    <div class="card-content grey lighten-4">
      <div id="test4">Tenemos la mejor calidad en nuestros productos desde que  abrieron las puertas nuestros restaurantes </div>
      <div id="test5">Productos frescos desde que iniciamos por las mañana para darte el mejor sabor en cada mordida</div>
      <div id="test6">Nuestros productos son para todo publica para que todos puedan degustar de el mejor sabor mexicano</div>
    </div>
          </div>
          </div>
         
          <div class="col s12 m6">
          <div class="card green">
            <div class="card-content white-text">
              <span class="card-title center">Eventos</span>
              <p>Quieres crear un evento con nosotros?.</p>
              <p>llena el siguiente formulario y nos pondremos en contacto para brindarte mas informacion al respecto.</p>
            </div>
                  
            <div class="card-tabs">
            <ul class="tabs tabs-fixed-width tabs-transparent white" >
              <li class="tab "><a class="green-text text-darken-2  active" href="#test7">Disponibilidad</a></li>
              <li class="tab"><a  class="green-text text-darken-2" href="#test8">Crea tus eventos</a></li>
            
            </ul>
          </div>
          <div class="card-content grey lighten-4">
            <div id="test7"> Contamos con una disponibilidad muy amplia ya que contamos eventos de lunes a domingo   </div>
            <div id="test8" class=" center" ><a class="waves-effect waves-light btn green shake-slow shake-constant shake-constant--hover " href="../main/login.php">Inicia sesion y crea tu evento </a>
</div>

</div>   




</div>


      </div>

      <script type="text/javascript" src="../js/raphael.min.js"></script>
    <script type="text/javascript" src="../js/raphael.icons.min.js"></script>
    <script type="text/javascript" src="../js/wheelnav.min.js"></script>

    <script type="text/javascript">

        window.onload = function () {

            var wheel = new wheelnav("wheelDiv");

            //This is the place for code snippets from the documentation -> http://wheelnavjs.softwaretailoring.net/documentation.html

            wheel.createWheel(["El Mejor Sabor", "Mas Calidad", "Los Parados","Mejores tacos" ]);
            wheel.navigateWheel(2);

            //Insert generated JavaScript code from here -> http://pmg.softwaretailoring.net

        };

    </script>

    <!-- Insert generated HTML5 code from here -> http://pmg.softwaretailoring.net -->
    <script type="text/javascript" src="../js/intro.js"></script>

<?php
Page::footer();
?>