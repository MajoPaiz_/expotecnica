<?php

require('../../fpdf/fpdf.php');
require("../../lib/database.php");
class PDF extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w)
{
	//Set the array of column widths
	$this->widths=$w;
}

function SetAligns($a)
{
	//Set the array of column alignments
	$this->aligns=$a;
}

function Row($data)
{
	//Calculate the height of the row
	$nb=0;
	for($i=0;$i<count($data);$i++)
		$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	$h=5*$nb;
	//Issue a page break first if needed
	$this->CheckPageBreak($h);
	//Draw the cells of the row
	for($i=0;$i<count($data);$i++)
	{
		$w=$this->widths[$i];
		$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
		//Save the current position
		$x=$this->GetX();
		$y=$this->GetY();
		//Draw the border
		
		$this->Rect($x,$y,$w,$h);

		$this->MultiCell($w,5,$data[$i],0,$a,'true');
		//Put the position to the right of the cell
		$this->SetXY($x+$w,$y);
	}
	//Go to the next line
	$this->Ln($h);
}

function CheckPageBreak($h)
{
	//If the height h would cause an overflow, add a new page immediately
	if($this->GetY()+$h>$this->PageBreakTrigger)
		$this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
	//Computes the number of lines a MultiCell of width w will take
	$cw=&$this->CurrentFont['cw'];
	if($w==0)
		$w=$this->w-$this->rMargin-$this->x;
	$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
	$s=str_replace("\r",'',$txt);
	$nb=strlen($s);
	if($nb>0 and $s[$nb-1]=="\n")
		$nb--;
	$sep=-1;
	$i=0;
	$j=0;
	$l=0;
	$nl=1;
	while($i<$nb)
	{
		$c=$s[$i];
		if($c=="\n")
		{
			$i++;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
			continue;
		}
		if($c==' ')
			$sep=$i;
		$l+=$cw[$c];
		if($l>$wmax)
		{
			if($sep==-1)
			{
				if($i==$j)
					$i++;
			}
			else
				$i=$sep+1;
			$sep=-1;
			$j=$i;
			$l=0;
			$nl++;
		}
		else
			$i++;
	}
	return $nl;
}
function Header()
{
    global $title;

    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Calculamos ancho y posición del título.
    $w = $this->GetStringWidth($title)+6;
    $this->SetX((210-$w)/2);
    // Colores de los bordes, fondo y texto
    $this->SetDrawColor(0,80,180);
    $this->SetFillColor(230,230,0);
    $this->SetTextColor(220,50,50);
    // Ancho del borde (1 mm)
    $this->SetLineWidth(1);
    // Título
    $this->Cell($w,9,$title,1,1,'C',true);
    // Salto de línea
    $this->Ln(10);
}
function Footer()
{
	$this->SetY(-15);
	$this->SetFont('Arial','B',8);
	$this->Cell(100,10,'Ingreso',0,0,'L');

}

}

	$id= $_GET['id'];



	$sqlp="SELECT * from clientes where codigo_cliente =  ?";
    $parametrop=array($id);
	



	$fila = Database::getrow($sqlp,$parametrop);

	$pdf=new PDF('P','mm','Letter');
	$title = 'Los Parados';
	$pdf->SetTitle($title);
	$pdf->Open();
    
    $pdf->AddPage();
	$pdf->SetMargins(20,20,20);
	$pdf->Image('logo2.jpg',170,8,35);
	$pdf->Ln(10);
	$pdf->SetDrawColor(0,80,180);
    $pdf->SetFillColor(230,230,0);
    $pdf->SetTextColor(220,50,50);
	$pdf->SetFont('Arial','B',12);	
    $pdf->Cell(0,6,'Nombre: '.$fila['nombres_cliente'].' '.$fila['apellidos_cliente'],0,1);
	$pdf->Cell(0,6,'Alias: '.$fila['alias'],0,1); 
	$pdf->Cell(10,10,date('d/m/Y  h:m a '),0,1);
	$pdf->Ln(10);
	
	$pdf->SetWidths(array(85, 80, 85, 70, 20));
	$pdf->SetFont('Arial','B',10);
	$pdf->SetFillColor(85,107,47);
    $pdf->SetTextColor(255);

		for($i=0;$i<1;$i++)
			{
				$pdf->Row(array('FECHA iNGRESO', 'HORA INGRESO' ));
			}
	



    $sqlcon="SELECT ingreso.fecha_ingreso, ingreso.hora_ingreso 
	FROM ingreso
	Inner Join clientes ON ingreso.codigo_cliente = clientes.codigo_cliente 	
	WHERE clientes.codigo_cliente = ?";
	$parametroscon=array($id);
	$data=Database::getrows($sqlcon,$parametroscon);

	$i=0;
foreach($data as $fila)
{
	$pdf->SetFont('Arial','',10);
	
	if($i%2 == 1)
	{
		$pdf->SetFillColor(153,255,153);
		$pdf->SetTextColor(0);
		$pdf->Row(array($fila['fecha_ingreso'], $fila['hora_ingreso']));
		$i++;
	}
	else
	{
		$pdf->SetFillColor(102,204,51);
		$pdf->SetTextColor(0);
		$pdf->Row(array($fila['fecha_ingreso'], $fila['hora_ingreso']));
		$i++;
	}
}
	
	

$pdf->Output();
?>